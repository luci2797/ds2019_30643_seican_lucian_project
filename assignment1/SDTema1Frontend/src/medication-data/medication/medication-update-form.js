import React from 'react';
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import 'react-dropdown/style.css'

class MedicationUpdateForm extends React.Component{
    

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                    medicationId:{
                        value: '',
                        valid: false,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },

                    idPatient:{
                        value: '',
                        valid: true,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },
                    name:{
                        value: '',
                        placeholder:'name',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },

                    start:{
                        value: '',
                        placeholder:'start date',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },
                    end:{
                        value: '',
                        placeholder:'end date',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },

                    sideEffects:{
                        value: '',
                        placeholder:'side effects',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1,
                            isRequired: true 
                        }
                    },

                    dosage:{
                        value: '',
                        placeholder:'dosage',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1,
                            isRequired: true 
                        }
                    },
                    

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updateMedication(medication){
        return API_USERS.updateMedication(medication, (result, status, error) =>{
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit(){

        let medication = {
            medicationId: this.state.formControls.medicationId.value,
            idPatient : this.state.formControls.idPatient.value,
            name: this.state.formControls.name.value,
            start: this.state.formControls.start.value,
            end: this.state.formControls.end.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };

        this.updateMedication(medication);
    }

    render(){
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Update Existing Medication</h1>
  
                <p> Medication Id: </p>
                <input name="medicationId"
                        type="number"
                        value={this.state.formControls.medicationId.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.medicationId.touched}
                        valid={this.state.formControls.medicationId.valid}
                />
                {this.state.formControls.medicationId.touched && !this.state.formControls.medicationId.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}
  
                <p> Patient Id: </p>
                <input name="idPatient"
                        type="number"
                        value={this.state.formControls.idPatient.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.idPatient.touched}
                        valid={this.state.formControls.idPatient.valid}
                />
                {this.state.formControls.idPatient.touched && !this.state.formControls.idPatient.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}
  
                <p> Name: </p>
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p> Start date: </p>
                <TextInput name="start"
                           placeholder={this.state.formControls.start.placeholder}
                           value={this.state.formControls.start.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.start.touched}
                           valid={this.state.formControls.start.valid}
                />

                <p> End date: </p>
                <TextInput name="end"
                           placeholder={this.state.formControls.end.placeholder}
                           value={this.state.formControls.end.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.end.touched}
                           valid={this.state.formControls.end.valid}
                />

                <p> Side Effects: </p>
                <TextInput name="sideEffects"
                           placeholder={this.state.formControls.sideEffects.placeholder}
                           value={this.state.formControls.sideEffects.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.sideEffects.touched}
                           valid={this.state.formControls.sideEffects.valid}
                />

                <p> Dosage: </p>
                <input     type="number"
                           name="dosage"
                           placeholder={this.state.formControls.dosage.placeholder}
                           value={this.state.formControls.dosage.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.dosage.touched}
                           valid={this.state.formControls.dosage.valid}
                />
  
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Update
                </Button>
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
  
            </form>
        );
    }

}

export default MedicationUpdateForm;
