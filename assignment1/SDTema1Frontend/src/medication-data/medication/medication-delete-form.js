import React from 'react';
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import 'react-dropdown/style.css'

class MedicationDeleteForm extends React.Component{
    

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                    id:{
                        value: '',
                        valid: false,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },                    

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    deleteMedication(medication){
        return API_USERS.deleteMedication(medication, (result, status, error) =>{
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit(){

        let medication = {
            id: this.state.formControls.id.value,
        };

        this.deleteMedication(medication);
    }

    render(){
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Delete Medication</h1>
  
                <p> Medication Id: </p>
                <input name="id"
                        type="number"
                        value={this.state.formControls.id.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.id.touched}
                        valid={this.state.formControls.id.valid}
                />
                {this.state.formControls.id.touched && !this.state.formControls.id.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}
  
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Delete
                </Button>
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
  
            </form>
        );
    }

}

export default MedicationDeleteForm;
