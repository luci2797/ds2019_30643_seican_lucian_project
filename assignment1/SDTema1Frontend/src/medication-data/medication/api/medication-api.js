import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_medications: '/medication/all',
    create_medication: "/medication/create",
    update_medication: "/medication/update",
    delete_medication: "/medication/delete",
    medication_plan: "/medication/plan"
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function createMedication(user, callback){
    let request = new Request(HOST.backend_api +endpoint.create_medication, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(user, callback){
    let request = new Request(HOST.backend_api +endpoint.update_medication, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(user, callback){
    let request = new Request(HOST.backend_api +endpoint.delete_medication, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function medicationPlan(user, callback){
    let request = new Request(HOST.backend_api +endpoint.medication_plan, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    getMedications,
    createMedication,
    updateMedication,
    deleteMedication,
    medicationPlan,
};