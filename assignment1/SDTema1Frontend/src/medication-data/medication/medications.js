import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"

import * as API_USERS from "./api/medication-api"
import MedicationCreateForm from './medication-create-form';
import MedicationUpdateForm from './medication-update-form';
import MedicationDeleteForm from './medication-delete-form';

const columns = [
    {
        Header:  'Medication Id',
        accessor: 'medicationId',
    },
    {
        Header: 'Patient Id',
        accessor: 'idPatient',
    },
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Start',
        accessor: 'start',
    },
    {
        Header: 'End',
        accessor: 'end',
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects',
    },

];
const filters = [
    {
        accessor: 'medicationId',
    },
    {
        accessor: 'idPatient',
    },
    {
        accessor: 'name',
    },
    {
        accessor: 'start',
    },
    {
        accessor: 'end',
    },
    {
        accessor: 'dosage',
    },
    {
        accessor: 'sideEffects',
    },

];

class Medications extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_USERS.getMedications((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       medicationId: x.medicationId,
                       idPatient: x.idPatient,
                       name: x.name,
                       start: x.start,
                       end: x.end,
                       dosage: x.dosage,
                       sideEffects: x.sideEffects
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationCreateForm createMedication ={this.refresh}>

                                </MedicationCreateForm>
                            </div>
                        </Card>
                    </Col>

                    <Col>
                        <Card body>
                            <div>
                                <MedicationUpdateForm updateDoctor ={this.refresh}>

                                </MedicationUpdateForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                 <MedicationDeleteForm deleteDoctor ={this.refresh}>

                                </MedicationDeleteForm>
                            </div>
                        </Card>
                    </Col>
              </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Medications;