import React from 'react';
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import UserForm from "./user-form"
import * as API_USERS from "./api/user-api"

const columns = [
    {
        Header: 'Id',
        accessor: 'id',
    },
    {
        Header:  'Username',
        accessor: 'username',
    },
    {
        Header: 'Password',
        accessor: 'password',
    },
    {
        Header: 'Role',
        accessor: 'role',
    },


];

const filters = [
    {
        accessor: 'username',
    },
    {
        accessor: 'password',
    },
    {
        accessor: 'role',
    },
    {
        accessor : 'id'
    }
];

class Users extends React.Component{
    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchUsers();
    }

    fetchUsers() {
        return API_USERS.getUsers((result, status, err)=>{
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.user_id,
                        username: x.username,
                        password: x.password,
                        role: x.role,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }
    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <UserForm registerUser={this.refresh}>

                                </UserForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };
    
}

export default Users;