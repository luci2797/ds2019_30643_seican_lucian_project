import React from 'react';
import validate from "./validators/user-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/user-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class UserForm extends React.Component{
    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

           formControls : {

               username: {
                   value: '',
                   placeholder: 'fill in a username',
                   valid: false,
                   touched: false,
                   validationRules: {
                       minLength: 3,
                       isRequired: true
                   }
               },

               password: {
                   value: '',
                   placeholder: 'password',
                   valid: false,
                   touched: false,
                   validationRules: {
                       minLength: 5,
                       isRequired: true
                   }
               },

               role: {
                   value: '',
                   placeholder: 'role',
                   valid: false,
                   touched: false,
                   validationRules: {
                        isRequired: true
                   }

               },
           }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerUser(user){
        return API_USERS.createUser(user, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted user with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }



    handleSubmit(){

        console.log("New user data:");
        console.log("Username: " + this.state.formControls.username.value);
        console.log("Password: " + this.state.formControls.password.value);
        console.log("Role: " + this.state.formControls.role.value);

        let user = {
            username: this.state.formControls.username.value,
            password : this.state.formControls.password.value,
            role: this.state.formControls.role.value
        };

        this.registerUser(user);
    }

    render() {
        return (

          <form onSubmit={this.handleSubmit}>

              <h1>Insert new user</h1>

              <p> Username: </p>

              <TextInput name="username"
                         placeholder={this.state.formControls.username.placeholder}
                         value={this.state.formControls.username.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.username.touched}
                         valid={this.state.formControls.username.valid}
              />
              {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
              <div className={"error-message row"}> * Name must have at least 3 characters </div>}

              <p> Password: </p>
              <TextInput name="password"
                         placeholder={this.state.formControls.password.placeholder}
                         value={this.state.formControls.password.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.password.touched}
                         valid={this.state.formControls.password.valid}
              />
              {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
              <div className={"error-message"}> * Password must have a valid format</div>}


              <p> Role: </p>
              <TextInput name="role"
                         placeholder={this.state.formControls.role.placeholder}
                         value={this.state.formControls.role.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.role.touched}
                         valid={this.state.formControls.role.valid}
              />

              <p></p>
              <Button variant="success"
                      type={"submit"}
                      >
                  Submit
              </Button>

              {this.state.errorStatus > 0 &&
              <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

          </form>

        );
    }
}

export default UserForm