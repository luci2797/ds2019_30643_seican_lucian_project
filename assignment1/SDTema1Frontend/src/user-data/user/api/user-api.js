import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_users: '/user/all',
    create_user: "/user/create"
};

function getUsers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_users, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function createUser(user, callback){
    let request = new Request(HOST.backend_api + endpoint.create_user , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    getUsers,
    createUser
};