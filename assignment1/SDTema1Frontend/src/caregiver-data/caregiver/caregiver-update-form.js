import React from 'react';
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'

const options = [
    'male', 'female'
  ]

const defaultOption = options[0]

class CaregiverUpdateForm extends React.Component{
    

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                    caregiverId:{
                        value: '',
                        valid: false,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },

                    idUser:{
                        value: '',
                        valid: true,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },

                    idDoctor:{
                        value: '',
                        valid: true,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },

                    name:{
                        value: '',
                        placeholder:'name',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },

                    birthDate:{
                        value: '',
                        placeholder:'birthdate',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },

                    gender:{
                        value: '',
                        placeholder:'gender',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1,
                            isRequired: true 
                        }
                    },

                    address:{
                        value: '',
                        placeholder:'address',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1,
                            isRequired: true 
                        }
                    },
                    

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updateCaregiver(caregiver){
        return API_USERS.updateCaregiver(caregiver, (result, status, error) =>{
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit(){

        let caregiver = {
            caregiverId: this.state.formControls.caregiverId.value,
            idUser : this.state.formControls.idUser.value,
            idDoctor : this.state.formControls.idDoctor.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            address: this.state.formControls.address.value,
            gender: this.state.formControls.gender.value,
        };

        this.updateCaregiver(caregiver);
    }

    render(){
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Update Existing Caregiver</h1>

                <p> Caregiver Id: </p>
                <input name="caregiverId"
                        type="number"
                        value={this.state.formControls.caregiverId.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.caregiverId.touched}
                        valid={this.state.formControls.caregiverId.valid}
                />
                {this.state.formControls.caregiverId.touched && !this.state.formControls.caregiverId.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}

  
                <p> User Id: </p>
                <input name="idUser"
                        type="number"
                        value={this.state.formControls.idUser.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.idUser.touched}
                        valid={this.state.formControls.idUser.valid}
                />
                {this.state.formControls.idUser.touched && !this.state.formControls.idUser.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}

                <p> Doctor Id: </p>
                <input name="idDoctor"
                        type="number"
                        value={this.state.formControls.idDoctor.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.idDoctor.touched}
                        valid={this.state.formControls.idDoctor.valid}
                />
                {this.state.formControls.idDoctor.touched && !this.state.formControls.idDoctor.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}
  
  
                <p> Name: </p>
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p> Birtdate: </p>
                <TextInput name="birthDate"
                           placeholder={this.state.formControls.birthDate.placeholder}
                           value={this.state.formControls.birthDate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.birthDate.touched}
                           valid={this.state.formControls.birthDate.valid}
                />

                <p> Gender: </p>
                <TextInput name="gender"
                           placeholder={this.state.formControls.gender.placeholder}
                           value={this.state.formControls.gender.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.gender.touched}
                           valid={this.state.formControls.gender.valid}
                />

                <p> Address: </p>
                <TextInput name="address"
                           placeholder={this.state.formControls.address.placeholder}
                           value={this.state.formControls.address.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.address.touched}
                           valid={this.state.formControls.address.valid}
                />
  
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Update
                </Button>
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
  
            </form>
        );
    }

}

export default CaregiverUpdateForm;
