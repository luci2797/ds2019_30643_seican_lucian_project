import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_caregivers: '/caregiver/all',
    create_caregiver: "/caregiver/create",
    update_caregiver: "/caregiver/update",
    delete_caregiver: "/caregiver/delete"
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregivers, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function createCaregiver(user, callback){
    let request = new Request(HOST.backend_api +endpoint.create_caregiver, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(user, callback){
    let request = new Request(HOST.backend_api +endpoint.update_caregiver, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(user, callback){
    let request = new Request(HOST.backend_api +endpoint.delete_caregiver, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    getCaregivers,
    createCaregiver,
    updateCaregiver,
    deleteCaregiver
};