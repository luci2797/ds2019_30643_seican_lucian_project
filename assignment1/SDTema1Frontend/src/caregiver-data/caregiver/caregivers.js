import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import CaregiverCreateForm from './caregiver-create-form'
import CaregiverUpdateForm from './caregiver-update-form'
import CaregiverDeleteForm from './caregiver-delete-form'


import * as API_USERS from "./api/caregiver-api"

const columns = [
    {
        Header:  'Caregiver Id',
        accessor: 'caregiverId',
    },
    {
        Header: 'Id User',
        accessor: 'idUser',
    },
    {
        Header: 'Id Doctor',
        accessor: 'idDoctor',
    },
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },

];

const filters = [
    {
        accessor: 'caregiverId',
    },
    {
        accessor: 'idUser',
    },
    {
        accessor: 'idDoctor',
    },
    {
        accessor: 'name',
    },
    {
        accessor: 'birthDate',
    },
    {
        accessor: 'gender',
    },
    {
        accessor: 'address',
    },

];

const options = [
    'male', 'female'
  ]

class Caregivers extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       caregiverId: x.caregiverId,
                       idUser: x.idUser,
                       idDoctor: x.idDoctor,
                       name: x.name,
                       birthDate: x.birthDate,
                       gender: x.gender,
                       address: x.address
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <CaregiverCreateForm createCaregiver ={this.refresh}>

                                </CaregiverCreateForm>
                            </div>
                        </Card>
                    </Col>

                    <Col>
                    <Card body>
                            <div>
                                <CaregiverUpdateForm updateCaregiver ={this.refresh}>

                                </CaregiverUpdateForm>
                            </div>
                        </Card>
                    </Col>
                </Row>
                <Row>
                <Col>
                        <Card body>
                            <div>
                                <CaregiverDeleteForm deleteCaregiver ={this.refresh}>

                                </CaregiverDeleteForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

               
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Caregivers;