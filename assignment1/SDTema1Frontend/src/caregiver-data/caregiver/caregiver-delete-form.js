import React from 'react';
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'


class CaregiverDeleteForm extends React.Component{
    

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                    id:{
                        value: '',
                        valid: false,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    deleteCaregiver(caregiver){
        return API_USERS.deleteCaregiver(caregiver, (result, status, error) =>{
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit(){

        let caregiver = {
            id: this.state.formControls.id.value,
        };

        this.deleteCaregiver(caregiver);
    }

    render(){
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Delete Existing Caregiver</h1>
  
                <p> Caregiver Id: </p>
                <TextInput name="id"
                        value={this.state.formControls.id.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.id.touched}
                        valid={this.state.formControls.id.valid}
                />
                {this.state.formControls.id.touched && !this.state.formControls.id.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}  
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Delete
                </Button>
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
  
            </form>
        );
    }

}

export default CaregiverDeleteForm;
