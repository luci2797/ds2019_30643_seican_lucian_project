import React from 'react';
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class DoctorUpdateForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                    doctorId:{
                        value: '',
                        placeholder:1,
                        valid: true,
                        touched: false,
                        validationRules:{
                            isRequired: true
                        }
                    },

                    idUser:{
                        value: '',
                        valid: true,
                        touched: false,
                        validationRules: {
                            isRequired: true,
                        }
                    },

                    firstName:{
                        value: '',
                        placeholder:'first name',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1, 
                            isRequired: true
                        }
                    },

                    lastName:{
                        value: '',
                        placeholder:'last name',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 1,
                            isRequired: true 
                        }
                    },
                    

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updateDoctor(doctor){
        return API_USERS.updateDoctor(doctor, (result, status, error) =>{
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit(){

        let doctor = {
            doctorId: this.state.formControls.doctorId.value,
            idUser : this.state.formControls.idUser.value,
            firstName: this.state.formControls.firstName.value,
            lastName: this.state.formControls.lastName.value
        };

        this.updateDoctor(doctor);
    }

    render(){
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Update Existing Doctor</h1>
                <p>Doctor Id:</p>
                <input name="doctorId"
                        type="number" 
                        value={this.state.formControls.doctorId.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.doctorId.touched}
                        valid={this.state.formControls.doctorId.valid}
                />
                {this.state.formControls.doctorId.touched && !this.state.formControls.doctorId.valid &&
                <div className={"error-message row"}></div>}
  
                <p> User Id: </p>
                <input name="idUser"
                        type="number"
                        value={this.state.formControls.idUser.value}
                        onChange={this.handleChange}
                        touched={this.state.formControls.idUser.touched}
                        valid={this.state.formControls.idUser.valid}
                />
                {this.state.formControls.idUser.touched && !this.state.formControls.idUser.valid &&
                <div className={"error-message"}> * User Id must have a valid format</div>}
  
  
                <p> First Name: </p>
                <TextInput name="firstName"
                           placeholder={this.state.formControls.firstName.placeholder}
                           value={this.state.formControls.firstName.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.firstName.touched}
                           valid={this.state.formControls.firstName.valid}
                />
  
                <p> Last Name: </p>
                <TextInput name="lastName"
                           placeholder={this.state.formControls.lastName.placeholder}
                           value={this.state.formControls.lastName.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.lastName.touched}
                           valid={this.state.formControls.lastName.valid}
                />
  
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Update
                </Button>
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
  
            </form>
        );
    }

}

export default DoctorUpdateForm;
