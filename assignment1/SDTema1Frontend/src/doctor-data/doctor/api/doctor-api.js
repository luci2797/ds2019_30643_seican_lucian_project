import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_doctors: '/doctor/all',
    create_doctor: "/doctor/create",
    update_doctor: "/doctor/update",
    delete_doctor: "/doctor/delete"
};

function getDoctors(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_doctors, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function createDoctor(user, callback){
    let request = new Request(HOST.backend_api +endpoint.create_doctor, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateDoctor(user, callback){
    let request = new Request(HOST.backend_api +endpoint.update_doctor, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteDoctor(user, callback){
    let request = new Request(HOST.backend_api +endpoint.delete_doctor, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    getDoctors,
    createDoctor,
    updateDoctor,
    deleteDoctor
};