import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import DoctorForm from "./doctor-form";
import DoctorUpdateForm from "./doctor-update-form";
import DoctorDeleteForm from "./doctor-delete-form";

import * as API_USERS from "./api/doctor-api"

const columns = [
    {
        Header:  'Doctor Id',
        accessor: 'doctorId',
    },
    {
        Header: 'First Name',
        accessor: 'firstName',
    },
    {
        Header: 'Last Name',
        accessor: 'lastName',
    },
    {
        Header: 'User Id',
        accessor: 'idUser',
    },

];

const filters = [
    {
        accessor:'doctorId',
    },
    {
        accessor:'firstName',
    },
    {
        accessor:'lastName',
    },
    {
        accessor:'idUser',
    },

];

class Doctors extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_USERS.getDoctors((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       doctorId: x.doctorId,
                       firstName: x.firstName,
                       lastName: x.lastName,
                       idUser: x.idUser,
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <DoctorForm createDoctor ={this.refresh}>

                                </DoctorForm>
                            </div>
                        </Card>
                    </Col>

                    <Col>
                        <Card body>
                            <div>
                                <DoctorUpdateForm updateDoctor ={this.refresh}>

                                </DoctorUpdateForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <DoctorDeleteForm deleteDoctor ={this.refresh}>

                                </DoctorDeleteForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Doctors;