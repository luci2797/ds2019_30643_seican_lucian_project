import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientCreateForm from './patient-create-form'
import PatientUpdateForm from './patient-update-form'

import * as API_USERS from "./api/patient-api"

const columns = [
    {
        Header:  'Patient Id',
        accessor: 'patientId',
    },
    {
        Header: 'Id User',
        accessor: 'idUser',
    },
    {
        Header: 'Id Caregiver',
        accessor: 'idCaregiver',
    },
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Medical record',
        accessor: 'medicalRecord',
    },

];

const filters = [
    {
        accessor: 'patientId',
    },
    {
        accessor: 'idUser',
    },
    {
        accessor: 'idCaregiver',
    },
    {
        accessor: 'name',
    },
    {
        accessor: 'birthDate',
    },
    {
        accessor: 'gender',
    },
    {
        accessor: 'medicalRecord',
    },

];


class Patients extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       patientId: x.patientId,
                       idUser: x.idUser,
                       idCaregiver: x.idCaregiver,
                       name: x.name,
                       birthDate: x.birthDate,
                       gender: x.gender,
                       medicalRecord: x.medicalRecord
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PatientCreateForm createPatient ={this.refresh}>

                                </PatientCreateForm>
                            </div>
                        </Card>
                    </Col>

                    <Col>
                    <Card body>
                            <div>
                                <PatientUpdateForm updatePatient ={this.refresh}>

                                </PatientUpdateForm>
                            </div>
                        </Card>
                    </Col>
                </Row>
                <Row>
                <Col>
                        <Card body>
                            <div>
                                {/*<CaregiverDeleteForm deleteCaregiver ={this.refresh}>

        </CaregiverDeleteForm> */}
                            </div>
                        </Card>
                    </Col>
                </Row>

               
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Patients;