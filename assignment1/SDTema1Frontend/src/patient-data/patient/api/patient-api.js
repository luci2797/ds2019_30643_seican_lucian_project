import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_patients: '/patient/all',
    create_patient: "/patient/create",
    update_patient: "/patient/update",
    delete_patient: "/patient/delete"
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function createPatient(user, callback){
    let request = new Request(HOST.backend_api +endpoint.create_patient, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(user, callback){
    let request = new Request(HOST.backend_api +endpoint.update_patient, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(user, callback){
    let request = new Request(HOST.backend_api +endpoint.delete_patient, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    getPatients,
    createPatient,
    updatePatient,
    deletePatient
};