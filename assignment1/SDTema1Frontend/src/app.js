import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Users from './user-data/user/users';
import Doctors from './doctor-data/doctor/doctors';
import Caregivers from './caregiver-data/caregiver/caregivers';
import Patients from './patient-data/patient/patients';
import Medications from './medication-data/medication/medications';

let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/persons'
                            render={() => <Persons/>}
                        />

                        <Route
                            exact
                            path='/user'
                            render={() => <Users/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <Doctors/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() => <Caregivers/>}
                        />

                        <Route
                            exact
                            path='/patient'
                            render={() => <Patients/>}
                        />

                        <Route
                            exact
                            path='/medication'
                            render={() => <Medications/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
