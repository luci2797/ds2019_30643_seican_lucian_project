package com.example.springdemo.dto;

public class PatientDTO {
    private Integer patientId;
    private Integer idCaregiver;
    private Integer idUser;
    private String name;
    private String birthDate;
    private String gender;
    private String medicalRecord;

    public PatientDTO(Integer patientId, Integer idCaregiver, Integer idUser, String name, String birthDate, String gender, String medicalRecord) {
        this.patientId = patientId;
        this.idCaregiver = idCaregiver;
        this.idUser = idUser;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(Integer idCaregiver) {
        this.idCaregiver = idCaregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
