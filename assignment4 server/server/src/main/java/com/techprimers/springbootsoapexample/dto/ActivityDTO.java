package com.techprimers.springbootsoapexample.dto;

import java.util.Date;

public class ActivityDTO {
    private String start;
    private String end;
    private String name;

    public ActivityDTO() {
    }

    public ActivityDTO(String start, String end, String name) {
        this.start = start;
        this.end = end;
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "start=" + start +
                ", end=" + end +
                ", name='" + name + '\'' +
                '}';
    }
}
