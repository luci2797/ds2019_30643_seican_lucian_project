package com.techprimers.springbootsoapexample.service;

import com.techprimers.springbootsoapexample.dto.PatientActivityDto;
import com.techprimers.springbootsoapexample.utils.QueueConnection;

import javax.jws.WebService;
import java.util.ArrayList;

@WebService
public class ActivitiesServiceImpl implements ActivitiesService {
    public ArrayList<PatientActivityDto> getActivitiesByUserId(int patientId) {
        ArrayList<PatientActivityDto> result = new ArrayList<>();
        for (PatientActivityDto p : QueueConnection.patientActivities) {
            if (p.getPatientId() == patientId) {
                result.add(p);
            }
        }
        return result;
    }
}
