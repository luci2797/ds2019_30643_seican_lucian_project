package com.techprimers.springbootsoapexample.endpoint;

import com.techprimers.spring_boot_soap_example.Activity;
import com.techprimers.spring_boot_soap_example.GetActivitiesRequest;
import com.techprimers.spring_boot_soap_example.GetActivitiesResponse;
import com.techprimers.springbootsoapexample.dto.PatientActivityDto;
import com.techprimers.springbootsoapexample.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;

@Endpoint
public class ActivityEndpoint {

    @Autowired
    private ActivityService activityService;

    @PayloadRoot(namespace = "http://techprimers.com/spring-boot-soap-example",
        localPart = "getActivitiesRequest")
    @ResponsePayload
    public GetActivitiesResponse getActivitiesRequest(@RequestPayload GetActivitiesRequest request) {
        GetActivitiesResponse response = new GetActivitiesResponse();
        ArrayList<PatientActivityDto> activities = activityService.getPatientActivities(request.getUserId());
        for (PatientActivityDto p : activities) {
            Activity activity = new Activity();
            activity.setActivityId(p.getId());
            activity.setUserId(p.getPatientId());
            activity.setStartDate(p.getStartDate());
            activity.setEndDate(p.getEndDate());
            activity.setDescription(p.getDescription());
            activity.setNotes(p.getNotes());
            response.getActivities().add(activity);
        }
        return response;
    }
}
