package com.techprimers.springbootsoapexample.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Conversions {
    public static Date stringToDate(String strDate){
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy, HH:mm:ss a");
        Date output = null;
        try {
            output = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output;
    }
}
