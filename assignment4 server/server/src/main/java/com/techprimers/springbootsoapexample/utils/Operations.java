package com.techprimers.springbootsoapexample.utils;

import com.techprimers.springbootsoapexample.dto.ActivityDTO;

import java.util.Date;
import java.util.Random;

public class Operations {
    private static int id = 0;
    public static int differenceInHours(Date d1, Date d2){
        final int MILLI_TO_HOUR = 1000 * 60 * 60;
        return (int) (d1.getTime() - d2.getTime()) / MILLI_TO_HOUR;
    }

    public static String getRule(ActivityDTO activity){
        Date start = Conversions.stringToDate(activity.getStart());
        Date end = Conversions.stringToDate(activity.getEnd());
        int length = Operations.differenceInHours(end,start);
        String name = activity.getName();
        if (name.equals("Sleeping") && length>12){
            return "R1";
        }
        else if (name.equals("Leaving") && length>12){
            return "R2";

        }
        else if(name.equals("Toileting") && length > 1){
            return "R3";
        }
        return "No rule";
    }

    public static int getNextId(){
        id++;
        return id;
    }

    public static int getPatientId(){
        Random r = new Random();
        return r.nextInt(10)+1;
    }
}
