package com.techprimers.springbootsoapexample.utils;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.techprimers.springbootsoapexample.dto.ActivityDTO;
import com.techprimers.springbootsoapexample.dto.PatientActivityDto;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class QueueConnection {
    public static ArrayList<PatientActivityDto> patientActivities = new ArrayList<>();

    public static void setupConnection() throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://guest:guest@localhost");
        factory.setConnectionTimeout(300000);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("my-queue", true, false, false, null);
        Gson gson = new Gson();

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume("my-queue", false, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();

            if (delivery != null) {
                try {
                    String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                    ActivityDTO activityDTO = gson.fromJson(message, ActivityDTO.class);
                    //System.out.println("Message consumed: " + activityDTO.toString());
                    String rule = Operations.getRule(activityDTO);
                    PatientActivityDto patientActivityDto = new PatientActivityDto(Operations.getNextId(),
                            Operations.getPatientId(),
                            activityDTO.getStart(),
                            activityDTO.getEnd(),
                            activityDTO.getName(),
                            rule);
                    patientActivities.add(patientActivityDto);
                   // System.out.println("////////////////////////");
                   // System.out.println("Rule applied:" + rule);
                    //System.out.println("////////////////////////");
                    // Interact with IO
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (Exception e) {
                    channel.basicReject(delivery.getEnvelope().getDeliveryTag(), true);
                }
            }
        }

    }
}
