package com.techprimers.springbootsoapexample;

import com.techprimers.springbootsoapexample.utils.QueueConnection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class SpringBootSoapExampleApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBootSoapExampleApplication.class, args);
		try {
			QueueConnection.setupConnection();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
