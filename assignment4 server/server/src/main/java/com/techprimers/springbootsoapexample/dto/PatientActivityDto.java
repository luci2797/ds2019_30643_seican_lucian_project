package com.techprimers.springbootsoapexample.dto;

public class PatientActivityDto {
    private Integer id;
    private Integer patientId;
    private String startDate;
    private String endDate;
    private String description;
    private String notes;

    public PatientActivityDto() {
    }

    public PatientActivityDto(Integer id, Integer patientId, String startDate, String endDate, String description, String notes) {
        this.id = id;
        this.patientId = patientId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
