package com.techprimers.springbootsoapexample.service;

import com.techprimers.springbootsoapexample.dto.PatientActivityDto;
import com.techprimers.springbootsoapexample.utils.QueueConnection;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

@Service
public class ActivityService {
    ArrayList<PatientActivityDto> patientActivities = new ArrayList<>();

    @PostConstruct
    public void initialize() throws URISyntaxException, IOException, NoSuchAlgorithmException, InterruptedException, KeyManagementException {
        QueueConnection.setupConnection();
        patientActivities = QueueConnection.patientActivities;
    }

    public ArrayList<PatientActivityDto> getPatientActivities(int patientId){
        ArrayList<PatientActivityDto> result = new ArrayList<>();
        for (PatientActivityDto p:patientActivities){
            if (p.getPatientId() == patientId){
                result.add(p);
            }
        }
        return result;
    }

}
