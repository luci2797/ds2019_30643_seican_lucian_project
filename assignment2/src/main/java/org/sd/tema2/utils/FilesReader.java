package org.sd.tema2.utils;

import org.sd.tema2.dto.ActivityDTO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FilesReader {
    public static List<ActivityDTO> getActivities(){
        ArrayList<ActivityDTO> activities = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("activity.txt"));
            String line = reader.readLine();
            while (line != null){
                System.out.println(line);
                String[] lineComponents = line.split("\t\t",-1);
                ActivityDTO activityDTO = new ActivityDTO(
                        DataConverter.stringToDate(lineComponents[0]),
                        DataConverter.stringToDate(lineComponents[1]),
                        lineComponents[2]
                );
                activities.add(activityDTO);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return activities;
    }
}
