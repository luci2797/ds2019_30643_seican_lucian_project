package org.sd.tema2.utils;

import org.sd.tema2.dto.ActivityDTO;

import java.util.Date;

public class Operations {
    public static int differenceInHours(Date d1, Date d2){
        final int MILLI_TO_HOUR = 1000 * 60 * 60;
        return (int) (d1.getTime() - d2.getTime()) / MILLI_TO_HOUR;
    }

    public static String getRule(ActivityDTO activity){
        int length = Operations.differenceInHours(activity.getEnd(), activity.getStart());
        String name = activity.getName();
        if (name.equals("Sleeping") && length>12){
            return "R1";
        }
        else if (name.equals("Leaving") && length>12){
            return "R2";

        }
        else if(name.equals("Toileting") && length > 1){
            return "R3";
        }
        return "No rule";
    }
}
