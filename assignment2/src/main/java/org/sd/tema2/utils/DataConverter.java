package org.sd.tema2.utils;

import org.sd.tema2.dto.ActivityDTO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataConverter {
    public static Date stringToDate(String stringDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date output = null;
        try {
            output = formatter.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output;
    }

   /*public static String serializeObject(Object o){
        String serializedObject = new String();
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(serializedObject);
            so.flush();
            serializedObject = bo.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        return serializedObject;
    }

    public static Object deserializeObject(String serializedObject){
        ActivityDTO activityDTO = new ActivityDTO();
        try {
            byte b[] = serializedObject.getBytes();
            ByteArrayInputStream bi = new ByteArrayInputStream(b);
            ObjectInputStream si = new ObjectInputStream(bi);
            activityDTO = (ActivityDTO) si.readObject();
        } catch (Exception e) {
            System.out.println(e);
        }
        return activityDTO;
    }*/

}
