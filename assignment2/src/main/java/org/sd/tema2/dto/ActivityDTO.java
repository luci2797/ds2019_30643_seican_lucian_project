package org.sd.tema2.dto;

import java.util.Date;

public class ActivityDTO {
    private Date start;
    private Date end;
    private String name;

    public ActivityDTO() {
    }

    public ActivityDTO(Date start, Date end, String name) {
        this.start = start;
        this.end = end;
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "start=" + start +
                ", end=" + end +
                ", name='" + name + '\'' +
                '}';
    }
}
