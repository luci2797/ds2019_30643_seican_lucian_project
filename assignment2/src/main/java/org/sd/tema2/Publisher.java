package org.sd.tema2;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.sd.tema2.dto.ActivityDTO;
import org.sd.tema2.utils.DataConverter;
import org.sd.tema2.utils.FilesReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Publisher {

    public static void main(String[] args) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://guest:guest@localhost");
        factory.setConnectionTimeout(300000);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        List<ActivityDTO> activities = FilesReader.getActivities();
        Gson gson = new Gson();

        channel.queueDeclare("my-queue", true, false, false, null);

        int count = 0;

        while (count < activities.size()) {
            String message = gson.toJson(activities.get(count));

            channel.basicPublish("", "my-queue", null, message.getBytes());
            count++;
            System.out.println("Published message: " + message);

            Thread.sleep(5000);
        }
    }
}
